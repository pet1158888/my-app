import React, { Component } from 'react';
class Header extends Component{
    constructor(props) {
        super();
        this.state = {date: new Date()};
        
        console.log('constr');
    }
    componentDidMount(){
        this.timeerID= setInterval(()=> this.tick(),1000)
        console.log('DidMount');
    }
    componentDidUpdate(){
        console.log('DIdupdate');
    }
    componentWillUnmount(){
        console.log('WillUnmount');
        clearInterval(this.timeerID);
    }

    tick(){
        this.setState({date: new Date()});
    }
    render(){
        return(
            <div>{this.state.date.toLocaleTimeString()}</div>
        )
    }
}
export default Header;