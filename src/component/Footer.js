import React from 'react'

const Footer = (props)=>{
    const {name,email}=props;
    return (
        <div>{name} {email}</div>
    )
}
export default Footer;