import React, { Component } from 'react';
import Courseitem from './component/course/courseitem';
import Footer from './component/Footer';
import Header from './component/Header';

class App extends Component {
  render(){
    return (
      <div>
        <Header />
        <Courseitem courseName='java' coursePrice='200' />
        <Footer name='Petch' email='phaichayon.2011@gmail.com' />
      </div>
    );
  }
}
export default App;
